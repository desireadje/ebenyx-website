/**
 * desireadje 2020-2021 arsh3y
 * Licensed under  ()
 */

// debugger // Fn + F10 pour debugger
$(document).ready(function(){
});

function confirmNewsLetters() {
	debugger
	var e = $('#email_newsletters_input').val().trim();
	if(e != '') {
		if(validateEmail(e) == true) {
			$.ajax({
				url : window.location.origin + "/ws/subscribe_newsletters/" + e,
		        type : 'GET',
				dataType : 'json',
				contentType: "application/json; charset=utf-8",
		        success: function(json) {
					// console.log(json);
					if(json.status == 1 && json.data == null) {
						msg_success(json.message);
						$('#email_newsletters_input').val("");
					} else if(json.status == 0 && json.data == null){
						msg_warning(json.message);
					} else {
						msg_error(json.message);
					}
		        },
		        error : function(data, status, error) {
					msg_error(data.responseJSON.error);
		            console.log(data);
		            console.log(status);
		            console.log(error);
		        }
		    });
		} else {
			var message = "Veuillez renseigner une adresse e-mail valide pour souscrire a la newsletters."
			msg_warning(message);
		}
	}
}

function validateEmail(email)  {
	try {
	   var regex = /\S+@\S+\.\S+/;
	    return regex.test(email);
	} catch (e) {
	   console.log(e);
	}
}
