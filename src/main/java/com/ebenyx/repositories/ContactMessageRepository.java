package com.ebenyx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.ContactMessage;

public interface ContactMessageRepository extends JpaRepository<ContactMessage, Long> {

	@Query("SELECT c FROM ContactMessage c WHERE c.etat = 1 AND c.enabled = true")
	List<ContactMessage> findAllItem();

}
