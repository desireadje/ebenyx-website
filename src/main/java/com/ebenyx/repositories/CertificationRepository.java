package com.ebenyx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.Certification;

public interface CertificationRepository extends JpaRepository<Certification, Long> {

	@Query("SELECT c FROM Certification c WHERE lower(c.nom) = ?1 AND c.enabled = true")
	Certification findItemByName(String nom);

	@Query("SELECT c FROM Certification c WHERE c.etat = 1 AND c.enabled = true ORDER BY c.idCertification DESC")
	List<Certification> findAllItems();

	@Query("SELECT c FROM Certification c WHERE c.idCertification = ?1 AND c.enabled = true")
	Certification findItemById(Long id);

}
