package com.ebenyx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.CategoriePartenaire;

public interface CategoriePartenaireRepository extends JpaRepository<CategoriePartenaire, Long> {

	@Query("SELECT c FROM CategoriePartenaire c WHERE lower(c.nom) = ?1 AND c.enabled = true")
	CategoriePartenaire findItemByName(String nom);

	@Query("SELECT c FROM CategoriePartenaire c WHERE c.etat = 1 ORDER BY c.idCategoriePartenaire DESC")
	List<CategoriePartenaire> findAllItems();

	@Query("SELECT c FROM CategoriePartenaire c WHERE c.idCategoriePartenaire = ?1")
	CategoriePartenaire findItemById(Long id);

}
