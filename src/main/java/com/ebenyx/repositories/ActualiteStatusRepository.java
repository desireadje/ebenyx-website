package com.ebenyx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.ActualiteStatus;

public interface ActualiteStatusRepository extends JpaRepository<ActualiteStatus, Long>{
	
	@Query("SELECT c FROM ActualiteStatus c WHERE lower(c.nom) = ?1")
	ActualiteStatus findByNom(String nom);
	
	@Query("SELECT c FROM ActualiteStatus c ORDER BY c.idActualiteStatus ASC")
	List<ActualiteStatus> findActualiteStatus();
	
	@Query("SELECT c FROM ActualiteStatus c ORDER BY c.nom ASC")
	List<ActualiteStatus> findActualiteStatusOrderByNomAsc();

}
