package com.ebenyx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.Sujet;

public interface SujetRepository extends JpaRepository<Sujet, Long> {
	
	@Query("SELECT c FROM Sujet c WHERE lower(c.nom) = ?1 AND c.enabled = true")
	Sujet findByNom(String nom);
	
	@Query("SELECT c FROM Sujet c WHERE c.etat = 1 AND c.enabled = true")
	List<Sujet> findAllItem();
	
	@Query("SELECT c FROM Sujet c WHERE c.idSujet = ?1 AND c.enabled = true")
	Sujet findItemById(Long id);

}
