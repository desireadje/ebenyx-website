package com.ebenyx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.Solution;

public interface SolutionRepository extends JpaRepository<Solution, Long> {

	@Query("SELECT c FROM Solution c WHERE lower(c.titre) = ?1 AND c.enabled = true")
	Solution findItemByName(String nom);

	@Query("SELECT c FROM Solution c WHERE c.etat = 1 AND c.enabled = true ORDER BY c.idSolution DESC")
	List<Solution> findAllItems();

	@Query("SELECT c FROM Solution c WHERE c.idSolution = ?1 AND c.enabled = true")
	Solution findItemById(Long id);

}
