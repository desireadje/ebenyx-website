package com.ebenyx.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ebenyx.entities.Realisation;

public interface RealisationRepository extends JpaRepository<Realisation, Long> {

}
