package com.ebenyx.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "certification")
public class Certification extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCertification;
	private String nom;
	private String description;

	private String photo;

	public Certification() {
		super();
	}

	public Certification(Long idCerficiation, String nom, String description, String photo) {
		super();
		this.idCertification = idCerficiation;
		this.nom = nom;
		this.description = description;
		this.photo = photo;
	}

	public Long getIdCertification() {
		return idCertification;
	}

	public void setIdCertification(Long idCerficiation) {
		this.idCertification = idCerficiation;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
