package com.ebenyx.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ebenyx.config.Constantes;

@Entity
@Table(name = "role")
public class Role implements Serializable {

	// Attributs
	@Id
	private String role;
	private String description;
	private int etat = 1;

	// Constructeurs
	public Role() {
		super();
	}

	public Role(String role, int etat) {
		super();
		this.role = role;
		this.etat = etat;
	}

	// Getters and Setters
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public static Role super_administrateur() {
		Role r = new Role();
		r.setRole(Constantes.ROLE_SUPER_ADMINISTRATEUR);
		r.setDescription("Super Administrateur de la plateforme");
		return r;
	}

	public static Role administrateur() {
		Role r = new Role();
		r.setRole(Constantes.ROLE_ADMINISTRATEUR);
		r.setDescription("Administrateur de la plateforme");
		return r;
	}

}
