package com.ebenyx.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.format.annotation.DateTimeFormat;

import com.ebenyx.config.Utils;

@Entity
@Table(name = "actualite")
public class Actualite extends BaseEntity {

	public static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
	private static final long serialVersionUID = 1L;

	// Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idActualite;
	private String referenceActualite; // slug de Actualite
	private String titreActualite;

	private String photoActualite;

	@Column(length = 500000)
	private String contenuActualite;
	@Column(length = 5000)
	private String resumeActualite;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateDeParution = new Date();
	private String tags;
	
	private String soeUrl;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idActualiteStatus", referencedColumnName = "idActualiteStatus")
	private ActualiteStatus idActualiteStatus;

	public Actualite() {
		super();
		this.referenceActualite = uuid();
	}

	public Actualite(String titreActualite, String photoActualite, String contenuActualite,
			String resumeActualite, Date dateDeParution, String tags, String soeUrl) {
		super();
		this.titreActualite = titreActualite;
		this.photoActualite = photoActualite;
		this.contenuActualite = contenuActualite;
		this.resumeActualite = resumeActualite;
		this.dateDeParution = dateDeParution;
		this.tags = tags;
		this.soeUrl = soeUrl;
	}

	public static SimpleDateFormat getSDF() {
		return SDF;
	}

	public static void setSDF(SimpleDateFormat sDF) {
		SDF = sDF;
	}

	public Long getIdActualite() {
		return idActualite;
	}

	public void setIdActualite(Long idActualite) {
		this.idActualite = idActualite;
	}

	public String getReferenceActualite() {
		return referenceActualite;
	}

	public void setReferenceActualite(String referenceActualite) {
		this.referenceActualite = referenceActualite;
	}

	public String getTitreActualite() {
		return titreActualite;
	}

	public void setTitreActualite(String titreActualite) {
		this.titreActualite = titreActualite;
	}

	public String getPhotoActualite() {
		return photoActualite;
	}

	public void setPhotoActualite(String photoActualite) {
		this.photoActualite = photoActualite;
	}

	public String getContenuActualite() {
		return contenuActualite;
	}

	public void setContenuActualite(String contenuActualite) {
		this.contenuActualite = contenuActualite;
	}

	public String getResumeActualite() {
		return resumeActualite;
	}

	public void setResumeActualite(String resumeActualite) {
		this.resumeActualite = resumeActualite;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public ActualiteStatus getIdActualiteStatus() {
		return idActualiteStatus;
	}

	public void setIdActualiteStatus(ActualiteStatus idActualiteStatus) {
		this.idActualiteStatus = idActualiteStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getDateDeParution() {
		return dateDeParution;
	}

	public String getSoeUrl() {
		return soeUrl;
	}

	public void setSoeUrl() {
		this.soeUrl = Utils.removeSpecialChar(titreActualite.toLowerCase());
	}

	public void setDateDeParution(Date dateDeParution) {
		try {
			String dateFormat = SDF.format(dateDeParution);
			System.out.println(dateFormat);
			dateDeParution = SDF.parse(dateFormat);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.dateDeParution = dateDeParution;
	}

	public void setReferenceActualite() {
		this.referenceActualite = uuid();
	}

	public String contenu_to_String() {
		try {
			// clean HTML using none whitelist (remove all HTML tags)
			String cleanedHTML = Jsoup.clean(contenuActualite, Whitelist.none());
			System.out.println("cleanedHTML = " + cleanedHTML);
			return cleanedHTML;
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}

	public Actualite brouillon() {
		Actualite actualite = new Actualite();
		actualite.setIdActualiteStatus(new ActualiteStatus().brouillon());
		return actualite;
	}

}
