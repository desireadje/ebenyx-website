package com.ebenyx.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "categorieProjet")
public class CategorieProjet implements Serializable {

	private static final long serialVersionUID = 1L;

	// Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCategorieProjet;
	private String nom;

	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateCreation = new Date();
	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateModification;

	private int etat = 1;
	private boolean enabled = Boolean.TRUE;

	public CategorieProjet() {
		super();
	}

	public CategorieProjet(String nom, Date dateCreation, Date dateModification, int etat, boolean enabled) {
		super();
		this.nom = nom;
		this.dateCreation = dateCreation;
		this.dateModification = dateModification;
		this.etat = etat;
		this.enabled = enabled;
	}

	public Long getIdCategorieProjet() {
		return idCategorieProjet;
	}

	public void setIdCategorieProjet(Long idCategorieProjet) {
		this.idCategorieProjet = idCategorieProjet;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
