package com.ebenyx.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "expertise")
public class Expertise extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idExpertise;
	private String nom;

	public Expertise() {
		super();
	}

	public Expertise(Long idExpertise, String nom) {
		super();
		this.idExpertise = idExpertise;
		this.nom = nom;
	}

	public Long getIdExpertise() {
		return idExpertise;
	}

	public void setIdExpertise(Long idExpertise) {
		this.idExpertise = idExpertise;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
