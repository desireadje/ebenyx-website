package com.ebenyx.entities;

import java.io.File;
import java.util.HashMap;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Mail {

	private String to;
	private String from;
	private String subject;
	private String content;
	private String cc;
	private String template;
	private HashMap<String, Object> model;
	private HashMap<String, File> attachement;

	public Mail() {
		this.content = content;
		model = new HashMap<>();
		attachement = new HashMap<>();
	}

	public String getTo() {
		return to;
	}

	public Mail setTo(String to) {
		this.to = to;
		return this;
	}

	public String getSubject() {
		return subject;
	}

	public Mail setSubject(String subject) {
		this.subject = subject;
		return this;
	}

	public String getContent() {
		return content;
	}

	public Mail setContent(String content) {
		this.content = content;
		return this;
	}

	public HashMap<String, Object> getModel() {
		return model;
	}

	public Mail setModel(HashMap<String, Object> model) {
		this.model = model;
		return this;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getTemplate() {
		return template;
	}

	public Mail setTemplate(String template) {
		this.template = template;
		return this;
	}

	public Mail setCC(String cc) {
		this.cc = cc;
		return this;
	}

	public String getCC() {
		return cc;
	}

	public Mail data(String key, Object value) {
		this.model.put(key, value);
		return this;
	}

	public Mail attachement(String name, File file) {
		this.attachement.put(name, file);
		return this;
	}

	public HashMap<String, File> getAttachement() {
		return attachement;
	}

	public static Mail createNew() {
		return new Mail();
	}

	public String getFrom() {
		return this.from;
	}

	public Mail setFrom(String from) {
		this.from = from;
		return this;
	}

}