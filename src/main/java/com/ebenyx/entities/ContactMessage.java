package com.ebenyx.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Entity
@Table(name = "contactMessage")
public class ContactMessage extends BaseEntity {

	private static final long serialVersionUID = 1L;

	// Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idContactMessage;
	private String reference;
	private String nomComplet;

	@Email
	private String email;
	private String telephone;

	@Column(length = 500000)
	private String message;
	private Boolean termeEtConditions = false;

	// Clés étrangères
	@ManyToOne
	@JoinColumn(name = "idSujet", referencedColumnName = "idSujet")
	private Sujet idSujet;

	// Constructeur
	public ContactMessage() {
		super();
		this.reference = uuid();
	}

	public ContactMessage(String nomComplet, @Email String email, String telephone, String message,
			boolean termeEtConditions, String reference) {
		super();
		this.nomComplet = nomComplet;
		this.email = email;
		this.telephone = telephone;
		this.message = message;
		this.termeEtConditions = termeEtConditions;
		this.reference = reference;
	}

	public Long getIdContactMessage() {
		return idContactMessage;
	}

	public void setIdContactMessage(Long idContactMessage) {
		this.idContactMessage = idContactMessage;
	}

	public String getNomComplet() {
		return nomComplet;
	}

	public void setNomComplet(String nomComplet) {
		this.nomComplet = nomComplet;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getTermeEtConditions() {
		return termeEtConditions;
	}

	public void setTermeEtConditions(Boolean termeEtConditions) {
		this.termeEtConditions = termeEtConditions;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = uuid();
	}

	public Sujet getIdSujet() {
		return idSujet;
	}

	public void setIdSujet(Sujet idSujet) {
		this.idSujet = idSujet;
	}

}
