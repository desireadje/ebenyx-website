package com.ebenyx.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ebenyx.config.Utils;

@Entity
@Table(name = "partenaires")
public class Partenaire extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPartenaire;
	private String nom;
	private String photo;

	@Column(length = 5000)
	private String resume;
	@Column(length = 500000)
	private String description;

	private String soeUrl;

	public Partenaire() {
		super();
	}

	public Partenaire(String nom, String photo, String resume, String description, String soeUrl) {
		super();
		this.nom = nom;
		this.photo = photo;
		this.resume = resume;
		this.description = description;
		this.soeUrl = soeUrl;
	}

	public Long getIdPartenaire() {
		return idPartenaire;
	}

	public void setIdPartenaire(Long idPartenaire) {
		this.idPartenaire = idPartenaire;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSoeUrl() {
		return soeUrl;
	}

	public void setSoeUrl() {
		this.soeUrl = Utils.removeSpecialChar(nom.toLowerCase());
	}

}
