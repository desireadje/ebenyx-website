package com.ebenyx.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ebenyx.config.Utils;

@Entity
@Table(name = "solution")
public class Solution extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSolution;
	private String titre;
	private String reference;

	@Column(length = 500000)
	private String description;

	@Column(length = 500000)
	private String details;

	private String photo;
	private String soeUrl;

	public Solution() {
		super();
		this.reference = uuid();
	}

	public Solution(String titre, String reference, String description, String details, String photo, String soeUrl) {
		super();
		this.titre = titre;
		this.reference = reference;
		this.description = description;
		this.details = details;
		this.photo = photo;
		this.soeUrl = soeUrl;
	}

	public Long getIdSolution() {
		return idSolution;
	}

	public void setIdSolution(Long idSolution) {
		this.idSolution = idSolution;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSoeUrl() {
		return soeUrl;
	}

	public void setSoeUrl() {
		this.soeUrl = Utils.removeSpecialChar(titre.toLowerCase());
	}

}
