package com.ebenyx.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "categoriePartenaire")
public class CategoriePartenaire implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCategoriePartenaire;
	private String nom;

	private int etat = 1;
	private boolean enabled = Boolean.TRUE;

	public CategoriePartenaire() {
		super();
	}

	public CategoriePartenaire(String nom, Date dateCreation, Date dateModification, int etat, boolean enabled) {
		super();
		this.nom = nom;
		this.etat = etat;
		this.enabled = enabled;
	}

	public Long getIdCategoriePartenaire() {
		return idCategoriePartenaire;
	}

	public void setIdCategoriePartenaire(Long idCategoriePartenaire) {
		this.idCategoriePartenaire = idCategoriePartenaire;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
