package com.ebenyx.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author desireadje
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateCreation = new Date();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "creerPar", referencedColumnName = "username")
	private Utilisateur creerPar;

	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateModification = new Date();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modifierPar", referencedColumnName = "username")
	private Utilisateur modifierPar;

	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateSuppression = new Date();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "supprimerPar", referencedColumnName = "username")
	private Utilisateur supprimerPar;

	private int etat = 1;
	private boolean enabled = Boolean.TRUE;

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String uuid() {
		return UUID.randomUUID().toString();
	}

	public Utilisateur getCreerPar() {
		return creerPar;
	}

	public void setCreerPar(Utilisateur creerPar) {
		this.creerPar = creerPar;
	}

	public Utilisateur getModifierPar() {
		return modifierPar;
	}

	public void setModifierPar(Utilisateur modifierPar) {
		this.modifierPar = modifierPar;
	}

	public Date getDateSuppression() {
		return dateSuppression;
	}

	public void setDateSuppression(Date dateSuppression) {
		this.dateSuppression = dateSuppression;
	}

	public Utilisateur getSupprimerPar() {
		return supprimerPar;
	}

	public void setSupprimerPar(Utilisateur supprimerPar) {
		this.supprimerPar = supprimerPar;
	}

}
