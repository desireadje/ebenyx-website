package com.ebenyx.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "realisation")
public class Realisation extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idRealisation;
	private String titre;
	private String reference;

	@Column(length = 500000)
	private String description;

	@Column(length = 500000)
	private String details;

	private String photo;

	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateDebut = new Date();

	// Clés étrangères
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "sujet_realisation",
		joinColumns = @JoinColumn(name = "idRealisation", referencedColumnName = "idRealisation"),
		inverseJoinColumns = @JoinColumn(name = "idSujet", referencedColumnName = "idSujet"))
	private List<Sujet> sujets;

	public Realisation() {
		super();
	}

	public Realisation(String titre, String reference, String description, String details, String photo,
			Date dateDebut) {
		super();
		this.titre = titre;
		this.reference = reference;
		this.description = description;
		this.details = details;
		this.photo = photo;
		this.dateDebut = dateDebut;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Long getIdRealisation() {
		return idRealisation;
	}

	public void setIdRealisation(Long idRealisation) {
		this.idRealisation = idRealisation;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = uuid();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public List<Sujet> getSujets() {
		return sujets;
	}

	public void setSujets(List<Sujet> sujets) {
		this.sujets = sujets;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
