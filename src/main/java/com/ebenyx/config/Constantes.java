package com.ebenyx.config;

public class Constantes {

	// PROTOCOL
	public static final String PROTOCOL = "http";
	public static final String DEFAULT_PORT = "80";

	// Description des differents roles
	public static final String ROLE_INTEGRATEUR = "integrateur";
	public static final String ROLE_SUPER_ADMINISTRATEUR = "super administrateur";
	public static final String ROLE_ADMINISTRATEUR = "administrateur";

	// Sexe
	public static final String MASCULAIN = "Masculin";
	public static final String FEMININ = "Féminin";

	// Les 5 continants
	public static final String CONTINENT_AFRIQUE = "Afrique";
	public static final String CONTINENT_AMERIQUE = "Amérique";
	public static final String CONTINENT_ANTARTIQUE = "Antarctique";
	public static final String CONTINENT_ASIE = "Asie";
	public static final String CONTINENT_EUROPE = "Europe";
	public static final String CONTINENT_OCEANIE = "Océanie";

	// Canvas color
	public static final String CANVAS_COLOR_AFRIQUE = "#f56954";
	public static final String CANVAS_COLOR_AMERIQUE = "#00a65a";
	public static final String CANVAS_COLOR_ANTARTIQUE = "#f39c12";
	public static final String CANVAS_COLOR_ASIE = "#00c0ef";
	public static final String CANVAS_COLOR_EUROPE = "#3c8dbc";
	public static final String CANVAS_COLOR_OCEANIE = "#6c757d";

	// Parametre SMTP par defaut (Dans la mesure ou le serveur de messagerie du
	// domaine ne fonctionne pas)
	public static final String DEFAULT_ADMIN_SYSTM_EMAIL = "adjedesire10@gmail.com";

	// Paramtres SMTP
	public static final String DEFAULT_SERVER_NAME = "Site Internet Ebenyx Technologie";
	public static final String DEFAULT_SMTP_HOST = "smtp.gmail.com";
	public static final String DEFAULT_SMTP_PORT = "465";

	public static final String DEFAULT_SMTP_EMAIL = "adesire@ebenyx.com";
	public static final String DEFAULT_SMTP_PASSWORD = "1234567890$";

	public static final String PWD_SPECIAL_CHARS = "[$@()%ù&#*-/^\\\\&£¨?<>\\[\\]!:+]";

	// Status des postes d'actualité
	public static final String ACTUALITE_STATUS_PUBLISHED = "Publié";
	public static final String ACTUALITE_STATUS_ATTENTE_EXEMEN = "En attente d'examen";
	public static final String ACTUALITE_STATUS_BROUILLON = "Brouillon";

	// Sujet
	public static final String SUJET_BLOCKCHAIN = "Blockchain";
	public static final String SUJET_KAFKA = "Kafka";
	public static final String SUJET_KUBERNATES = "Kubernates";

	// Longeur du resumé d'un article par defaut
	public static final Long LONGUEUR_RESUME_ARTICLE = (long) 199;

	// Fondateur
	public static final String NOM_FONDATEEUR = "Hervé Guéde";
	public static final String MOT_FONDATEUR = "Les enjeux liés à la digitalisation sont de plus en plus importants."
			+ "J’ai fondé Ebenyx avec la volonté de pouvoir accompagner au mieux les acteurs publics comme privés dans leur digitalisation.";

	// Localisation
	public static final String LOCALISATION = "Plateaux, cité Esculape Cocody  Abidjan, Côte d'Ivoire";
	public static final String GEO_LOCALISATION = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.616507700554!2d-4.017594085235103!3d5.322361096139057!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1ebdcda9abb3f%3A0x348ba51b56a92543!2sLa%20cit%C3%A9%20Esculape!5e0!3m2!1sfr!2sus!4v1626254275097!5m2!1sfr!2sus";

	// Contacts & Lien sociaux.
	public static final String EMAIL_CONTACT_EBT = "contact@ebenyx.com";
	public static final String PAGE_FACEBOOK = "https://www.facebook.com/EbenyxTechnologies/";
	public static final String PAGE_LINKEDIN = "https://www.linkedin.com/company/ebenyxtechnologies";
	public static final String PAGE_TWITTER = "https://twitter.com/?lang=fr";
	public static final String PAGE_YOUTUBE = "https://www.youtube.com";
	public static final String NUM_WHATSAPP = "002250777867451";

	// Fichiers & Images par defaut
	public static final String DEFAULT_PHOTO = "user_sample.jpeg";
	public static final String DEFAULT_BLOG_IMG = "defaut_blog.jpg";
	public static final String DEFAULT_BROCHURE = "PLAQUETTE NUMERIQUE EBENYX TECHNOLOGIES.pdf";
	public static final String DEFAUT_SOLUTION_IMG = "defaut_solution.jpg";
	public static final String DEFAULT_REALISATION_IMG = "defaut_realisation.jpg";
	public static final String DEFAULT_PARTENAIRE_IMG = "defaut_partenaire.jpg";
	public static final String DEFAULT_CERTIFICATION_IMG = "defaut_certification.jpg";

}