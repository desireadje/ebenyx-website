package com.ebenyx.config;

public class Msg {
	
	// Messages actions
	public static final String ITEM_EXISTE = "Cet information existe déja en base";
	
	public static final String DMD_SEND_SUCCESS = "Demande envoyée avec succès.";
	public static final String DMD_SEND_ERROR = "Erreur lors de l'envoi de la demande, veuillez réessayer à nouveau.";
	
	public static final String CREATE_SUCCESS = "Enregristrement effectué avec succès.";
	public static final String CREATE_ERROR = "Erreur lors de l'enregristrement, veuillez réessayer à nouveau.";

	public static final String UPDATE_SUCCESS = "Mise à jour effectuée avec succès";
	public static final String UPDATE_ERROR = "Erreur lors de la mise à jour, veuillez réessayer à nouveau.";

	public static final String REMOVE_SUCCESS = "Suppression effectuée avec succès";
	public static final String REMOVE_ERROR = "Erreur lors de la suppression, veuillez réessayer à nouveau.";

	public static final String ELEMENT_EN_BASE = "Cette élément est déja en base !";
	public static final String PASSWORD_ERROR = "Erreur Mot de passe incorrect, veuillez réessayer à nouveau.";

	public static final String SUCCESS = "Traitement effectué avec succès.";
	public static final String ERROR = "Erreur lors du traitement, veuillez réessayer à nouveau.";
	public static final String UNAUTHORIZE = "Accès non autorisisé.";

}
