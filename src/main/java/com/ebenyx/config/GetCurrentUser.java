package com.ebenyx.config;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class GetCurrentUser {

	public String getUserConnected() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentusername = null;
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			currentusername = authentication.getName();
		}
		return currentusername;
	}
}