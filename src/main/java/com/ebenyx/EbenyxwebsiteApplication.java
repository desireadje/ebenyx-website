package com.ebenyx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ebenyx.config.Constantes;
import com.ebenyx.config.EmailUtility;
import com.ebenyx.entities.ActualiteStatus;
import com.ebenyx.entities.Parametre;
import com.ebenyx.entities.Role;
import com.ebenyx.entities.Sujet;
import com.ebenyx.entities.Utilisateur;
import com.ebenyx.repositories.ActualiteStatusRepository;
import com.ebenyx.repositories.ParametreRepository;
import com.ebenyx.repositories.RoleRepository;
import com.ebenyx.repositories.SujetRepository;
import com.ebenyx.repositories.UtilisateurRepository;
import com.ebenyx.service.EmailService;

// @EnableScheduling
@SpringBootApplication
public class EbenyxwebsiteApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(EbenyxwebsiteApplication.class);

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private ParametreRepository parametreRepos;
	@Autowired
	private RoleRepository roleRepos;
	@Autowired
	private UtilisateurRepository utilisateurRepos;
	@Autowired
	private SujetRepository sujetRepos;
	@Autowired
	private ActualiteStatusRepository actualiteStatusRepos;
	
	@Autowired
	EmailService emailService;

	@Value("${server.address}")
	private String addressServer;

	@Value("${server.port}")
	private String portServer;

	@Value("${dir.images}")
	private String imagesDirectory;

	@Value("${dir.photos}")
	private String photosDirectory;

	@Value("${dir.blog}")
	private String blogDirectory;

	@Value("${dir.client}")
	private String clientDirectory;

	@Value("${dir.solution}")
	private String solutionDirectory;

	@Value("${dir.realisation}")
	private String realisationDirectory;
	
	@Value("${dir.partenaire}")
	private String partenaireDirectory;
	
	@Value("${dir.certification}")
	private String certificationDirectory;

	@Value("${dir.brochure}")
	private String brochureDirectory;

	EmailUtility emailUtility = new EmailUtility();
	
	

	public static void main(String[] args) {
		SpringApplication.run(EbenyxwebsiteApplication.class, args);
	}

	/**
	 * Cette fonction permet d'encoder les mots de passe en BCrypt
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void run(String... args) throws Exception {
		try {
			// TODO Debug mode : fn F6
			showPath();
			initData();
		} catch (Exception e) {
			log.error("Une erreur s'est produite lors de l'initialisation des données");
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public void showPath() {
		System.out.println("photosDirectory = " + photosDirectory);
		System.out.println("blogDirectory = " + blogDirectory);
		System.out.println("clientDirectory = " + clientDirectory);
		System.out.println("solutionDirectory = " + solutionDirectory);
		System.out.println("realisationDirectory = " + realisationDirectory);
		System.out.println("brochureDirectory = " + brochureDirectory);
		System.out.println("partenaireDirectory = " + partenaireDirectory);
		System.out.println("certificationDirectory = " + certificationDirectory);
	}

	private void initData() {
		log.info("Début d'initalisation des données applicative");

		configParams();
		createDefaultRole();
		createDefaultUser();

		createDefaultSujet();

		createDefaultActualiteStatus();

		log.info("Fin d'initalisation des données applicative");
	}

	private void createDefaultActualiteStatus() {
		if (actualiteStatusRepos.findByNom(Constantes.ACTUALITE_STATUS_PUBLISHED.toLowerCase()) == null) {
			ActualiteStatus c = new ActualiteStatus();
			c.setNom(Constantes.ACTUALITE_STATUS_PUBLISHED);
			actualiteStatusRepos.save(c);
		}

		if (actualiteStatusRepos.findByNom(Constantes.ACTUALITE_STATUS_ATTENTE_EXEMEN.toLowerCase()) == null) {
			ActualiteStatus c = new ActualiteStatus();
			c.setNom(Constantes.ACTUALITE_STATUS_ATTENTE_EXEMEN);
			actualiteStatusRepos.save(c);
		}

		if (actualiteStatusRepos.findByNom(Constantes.ACTUALITE_STATUS_BROUILLON.toLowerCase()) == null) {
			ActualiteStatus c = new ActualiteStatus();
			c.setNom(Constantes.ACTUALITE_STATUS_BROUILLON);
			actualiteStatusRepos.save(c);
		}
	}

	private void createDefaultSujet() {
		if (sujetRepos.findByNom(Constantes.SUJET_BLOCKCHAIN.toLowerCase()) == null) {
			Sujet c = new Sujet();
			c.setNom(Constantes.SUJET_BLOCKCHAIN);
			sujetRepos.save(c);
		}
		if (sujetRepos.findByNom(Constantes.SUJET_KAFKA.toLowerCase()) == null) {
			Sujet c = new Sujet();
			c.setNom(Constantes.SUJET_KAFKA);
			sujetRepos.save(c);
		}
		if (sujetRepos.findByNom(Constantes.SUJET_KUBERNATES.toLowerCase()) == null) {
			Sujet c = new Sujet();
			c.setNom(Constantes.SUJET_KUBERNATES);
			sujetRepos.save(c);
		}
	}

	private void configParams() {
		Parametre p = null;
		try {
			p = parametreRepos.get();
			if (p == null) {
				p = new Parametre().get();
				p.setBaseUrl(addressServer + ":" + portServer);
				// Insert des parametres par defaut
				parametreRepos.save(p);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}

	private void createDefaultRole() {
		try {
			if (roleRepos.findRole(Role.super_administrateur().getRole().toLowerCase()) == null) {
				roleRepos.save(Role.super_administrateur());
			}

			if (roleRepos.findRole(Role.administrateur().getRole().toLowerCase()) == null) {
				roleRepos.save(Role.administrateur());
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}

	private void createDefaultUser() {
		try {
			Utilisateur user = utilisateurRepos.save(Utilisateur
					.superAdministrateur(roleRepos.findRole(Constantes.ROLE_SUPER_ADMINISTRATEUR), passwordEncoder));
			
			// emailService.sendMail(user);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}
}
