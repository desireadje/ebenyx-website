package com.ebenyx.controllers.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ebenyx.config.Constantes;
import com.ebenyx.config.GetCurrentUser;
import com.ebenyx.config.Msg;
import com.ebenyx.entities.ClientEbenyx;
import com.ebenyx.entities.Solution;
import com.ebenyx.entities.Utilisateur;
import com.ebenyx.repositories.ClientEbenyxRepository;
import com.ebenyx.repositories.ParametreRepository;
import com.ebenyx.repositories.SolutionRepository;
import com.ebenyx.repositories.UtilisateurRepository;

@Controller
@RequestMapping(value = "/solution_ebenyx")
public class SolutionsController {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(SolutionsController.class);

	@Autowired
	private ParametreRepository parametreRepos;
	@Autowired
	private ClientEbenyxRepository clientEbenyxRepos;
	@Autowired
	private UtilisateurRepository utilisateurRepos;
	@Autowired
	private SolutionRepository solutionRepos;

	// Je cré une nouvelle instance de mon objet qui recupère l'utilisateur connecté
	GetCurrentUser user = new GetCurrentUser();
	Utilisateur userConnected = null;
	
	@Value("${dir.solution}")
	private String solutionDirectory;

	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "redirect:/login";
		try {
			if (user.getUserConnected() == null) {
				return page;
			}
			
			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			session.setAttribute("Utilisateur", userConnected);
			
			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {

				// J'active le menu
				model.addAttribute("active_menu", "solutions");
				model.addAttribute("page_title", "Solutions Ebenyx");

				model.addAttribute("Solutions", solutionRepos.findAllItems());

				page = "admin/solultions/index";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}
	
	
	/**
	 * Fonction form create new iten
	 */
	@GetMapping(value = "/formcreate")
	public String formcreate(Model model, HttpSession session) {
		String page = "redirect:/administrateurs";
		try {
			// Je recherche toutes les informations de l'utilisateur connecté
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());

			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {

				// J'envoi cet utilisateur dans ma vue
				session.setAttribute("Utilisateur", userConnected);

				// J'active le menu
				model.addAttribute("active_menu", "solutions");
				model.addAttribute("page_title", "Solutions Ebenyx");

				model.addAttribute("Solution", new Solution());

				page = "admin/solultions/create";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}
	
	/**
	 * Fonction create item
	 */
	@PostMapping(value = "/create")
	public String create(@Valid Solution a,
			@RequestParam(name = "picture") MultipartFile picture,
			BindingResult bindingResult, HttpSession session,
			RedirectAttributes redirAttrs, Model model) {
		Solution r = null;
		String page = "redirect:/solution_ebenyx";
		try {
			if (bindingResult.hasErrors()) {
				redirAttrs.addFlashAttribute("error", Msg.CREATE_ERROR);
				return page;
			}
			
			r = findOne(a.getTitre().toLowerCase());
			if (r != null) {
				redirAttrs.addFlashAttribute("error", Msg.ITEM_EXISTE);
				return page;
			}
			
			a.setSoeUrl();
			// image de mise en avant
			if (!(picture.isEmpty())) {
				a.setPhoto(picture.getOriginalFilename());
				picture.transferTo(new File(solutionDirectory + picture.getOriginalFilename()));
			}

			r = solutionRepos.save(a);
			
			if (r == null)
				redirAttrs.addFlashAttribute("error", Msg.CREATE_ERROR);
			else
				redirAttrs.addFlashAttribute("success", Msg.CREATE_SUCCESS);

		} catch (Exception e) {
			redirAttrs.addFlashAttribute("error", Msg.CREATE_ERROR);
			e.printStackTrace();
			log.error(e.getMessage());
			return page;
		}

		return page;
	}
	
	/**
	 * Fonction find one item
	 */
	public Solution findOne(String name) {
		Solution a = null;
		try {
			a = solutionRepos.findItemByName(name);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return a;
	}
	
	/**
	 * Fonction form update item
	 */
	@GetMapping(value = "/formupdate")
	public String formupdate(Long id, RedirectAttributes redirAttrs, Model model, HttpSession session) {
		String page = "redirect:/solution_ebenyx";
		try {
			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			
			if (userConnected == null) {
				return page;
			}
			
			// Si l'utilisateur connecté a le ROLE_PARTICULIER
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {
				// J'envoi cet utilisateur dans ma vue
				session.setAttribute("Utilisateur", userConnected);
				
				Solution t = null;
				t = solutionRepos.findItemById(id);
				
				if (t == null) {
					return page;
				}
				
				// J'active le menu
				model.addAttribute("active_menu", "solutions");
				model.addAttribute("page_title", "Solutions Ebenyx");

				model.addAttribute("ClientEbenyx", t);
				page = "admin/solultions/update";
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return page;
	}
	
	/**
	 * Fonction update item
	 */
	@PostMapping(value = "/update")
	public String update(@Valid Solution a, @RequestParam(name = "picture") MultipartFile picture,
			BindingResult bindingResult, HttpSession session, RedirectAttributes redirAttrs, Model model) {
		String page = "redirect:/solution_ebenyx";
		Solution t = null;
		try {
			if (bindingResult.hasErrors()) {
				redirAttrs.addFlashAttribute("error", Msg.CREATE_ERROR);
				return page;
			}
			
			if (user.getUserConnected() == null) {
				return page;
			}
			
			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			session.setAttribute("Utilisateur", userConnected);
			
			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {
				
				t = solutionRepos.findItemById(a.getIdSolution());
				if (t != null) {
					
					a.setCreerPar(a.getCreerPar());
					a.setModifierPar(userConnected);
					a.setDateModification(new Date());
					a.setSoeUrl();
					
					// logo
					if (!(picture.isEmpty())) {
						a.setPhoto(picture.getOriginalFilename());
						picture.transferTo(new File(solutionDirectory + picture.getOriginalFilename()));
					} else {
						a.setPhoto(a.getPhoto());
					}
					if (!(picture.isEmpty())) {
						a.setPhoto(picture.getOriginalFilename());
						picture.transferTo(new File(solutionDirectory + picture.getOriginalFilename()));
					} else {
						a.setPhoto(t.getPhoto());
					}
					
					
					t = solutionRepos.save(a);
				}
				
				if (t != null) {
					redirAttrs.addFlashAttribute("success", Msg.UPDATE_SUCCESS);
				} else {
					redirAttrs.addFlashAttribute("error", Msg.UPDATE_ERROR);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}
	
	/**
	 * Fonction active/desactive item
	 */
	@GetMapping(value = "/active")
	public String active(Long id, Model model, HttpSession session, RedirectAttributes redirAttrs) {
		String page = "redirect:/client";
		ClientEbenyx retour = null;
		try {
			// Je recherche toutes les informations de l'utilisateur connecté
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());

			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {

				// J'envoi cet utilisateur dans ma vue
				session.setAttribute("Utilisateur", userConnected);
				ClientEbenyx t = null;

				t = clientEbenyxRepos.findItemById(id);
				if (t != null) {
					if (t.getEtat() == 0) {
						t.setEtat(1);
					} else {
						t.setEtat(0);
					}
					t.setDateModification(new Date());
					retour = clientEbenyxRepos.save(t);
				}

				if (retour != null) {
					if (retour.getEtat() == 1) {
						redirAttrs.addFlashAttribute("success", Msg.SUCCESS);
					} else {
						redirAttrs.addFlashAttribute("success", Msg.SUCCESS);
					}
				} else {
					redirAttrs.addFlashAttribute("error", Msg.ERROR);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return page;
	}
	
	/**
	 * Cette fonction permet de recuperer la photo
	 */
	@GetMapping(value = "/getphoto", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhoto(String filename) throws FileNotFoundException, IOException {
		File file = new File(solutionDirectory + filename); // Je recupère l'image
		if (!file.exists()) {
			// photo par defaut
			String photo_defaut = parametreRepos.findSolutionImageDefaut();
			file = new File(solutionDirectory + photo_defaut);
		}
		return IOUtils.toByteArray(new FileInputStream(file));
	}
	
	
}
