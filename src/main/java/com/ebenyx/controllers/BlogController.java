package com.ebenyx.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ebenyx.config.Constantes;
import com.ebenyx.entities.Actualite;
import com.ebenyx.entities.ActualiteStatus;
import com.ebenyx.repositories.ActualiteRepository;
import com.ebenyx.repositories.ActualiteStatusRepository;
import com.ebenyx.repositories.ParametreRepository;

/**
 * @author Branislav Lazic
 */
@Controller
@RequestMapping(value = "/blog")
public class BlogController {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(BlogController.class);

	@Autowired
	private ParametreRepository parametreRepos;
	@Autowired
	private ActualiteRepository actualiteRepos;
	@Autowired
	private ActualiteStatusRepository actualiteStatusRepos;

	@Value("${dir.blog}")
	private String blogDirectory;
	
	@GetMapping()
	public String page(HttpSession session, HttpServletRequest request, Model model) {
		String view = "redirect:/";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Notre actualité");
			model.addAttribute("active_menu", "blog");

			session.setAttribute("Parametre", parametreRepos.get());

			int page = 0; // le numéro de page par défaut est 0 (oui c'est bizarre)
			int size = 6; // la taille de page par défaut est 10

			if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
				page = Integer.parseInt(request.getParameter("page")) - 1;
			}

			if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
				size = Integer.parseInt(request.getParameter("size"));
			}

			ActualiteStatus published = actualiteStatusRepos
					.findByNom(Constantes.ACTUALITE_STATUS_PUBLISHED.toLowerCase());
			Page<Actualite> actualitePages = null;
			actualitePages = actualiteRepos.findLastsActuOfBlog(PageRequest.of(page, size), published.getIdActualiteStatus());
			
			model.addAttribute("Actualites", actualitePages);
			
			view = "blog/index";

		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return view;

		// ModelAndView modelAndView = new ModelAndView("persons");// vue

		/*
		 * Évaluer la taille de la page. Si le paramètre demandé est nul, renvoie la
		 * taille de page initiale
		 */
		// int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		/*
		 * Si un paramètre demandé est nul ou inférieur à 1, renvoie la taille initiale.
		 * Sinon, renvoie la valeur de param. diminué de 1.
		 */
		// int evalPage = page.filter(p -> p >= 1).map(p -> p - 1).orElse(INITIAL_PAGE);

		// Page actualites = actualiteService.findAllPageable(PageRequest.of(evalPage,
		// evalPageSize));
		// Pager pager = new Pager(actualites.getTotalPages(), actualites.getNumber(),
		// BUTTONS_TO_SHOW);

		// Injection dans la vue.
		// titre et breadcrumb de la page
		/*
		 * modelAndView.addObject("page_title", "Notre actualité");
		 * modelAndView.addObject("active_menu", "blog");
		 * 
		 * modelAndView.addObject("Actualites", actualites);
		 * modelAndView.addObject("selectedPageSize", evalPageSize);
		 * modelAndView.addObject("pageSizes", PAGE_SIZES);
		 * modelAndView.addObject("pager", pager);
		 */

		// return modelAndView;
	}

	/**
	 * Fonction permettant de consulter un actualite du blog
	 */
	@GetMapping(value = "/view/{soeUrl}")
	public String view(@PathVariable("soeUrl") String soeUrl, RedirectAttributes redirAttrs, Model model,
			HttpSession session) {
		String page = "redirect:/actualite";
		try {
			session.setAttribute("Parametre", parametreRepos.get());

			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Notre actualité");
			model.addAttribute("active_menu", "blog");

			// Je recupere l'acticle publié à lire
			ActualiteStatus published = actualiteStatusRepos
					.findByNom(Constantes.ACTUALITE_STATUS_PUBLISHED.toLowerCase());
			Actualite actualiteALire = actualiteRepos.findActualiteBySoeUrlAndStatus(soeUrl, published);

			if (actualiteALire == null) {
				return "error/404";
			}

			model.addAttribute("ActualiteALire", actualiteALire);

			// Previous Post
			Actualite previous_post = null;
			if (actualiteALire.getIdActualite() > 1) {
				previous_post = actualiteRepos.findBlogNav(actualiteALire.getIdActualite() - 1, published);
			}
			model.addAttribute("PreviousPost", previous_post);

			// Next Post
			Actualite next_post = null;
			if (actualiteALire.getIdActualite() < actualiteRepos.findAll().size()) {
				next_post = actualiteRepos.findBlogNav(actualiteALire.getIdActualite() + 1, published);
			}
			model.addAttribute("NextPost", next_post);

			// Je recupere la liste du reste de l'actualité publié sans actualiteALire
			List<Actualite> resteActualites = null;
			resteActualites = actualiteRepos.findResteActualiteByStatus(actualiteALire.getIdActualite(), published);
			model.addAttribute("ResteActualites", resteActualites);

			page = "blog/view";

		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}

	/**
	 * Fonction get picture
	 */
	@GetMapping(value = "/getphotogrid", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhoto(String originalFilename) throws FileNotFoundException, IOException {
		File file = new File(blogDirectory + originalFilename); // Je recupère l'image
		if (!file.exists()) {
			// photo par defaut
			String blog_image_defaut = parametreRepos.findBlogImageDefaut();
			file = new File(blogDirectory + blog_image_defaut);
		}
		return IOUtils.toByteArray(new FileInputStream(file));
	}

}
