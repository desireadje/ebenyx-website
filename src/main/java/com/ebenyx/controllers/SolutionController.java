package com.ebenyx.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ebenyx.repositories.ParametreRepository;
import com.ebenyx.repositories.SolutionRepository;

@Controller
@RequestMapping(value = "/solutions")
public class SolutionController {
	
	// Logger
	private static final Logger log = LoggerFactory.getLogger(SolutionController.class);
	
	@Autowired
	private ParametreRepository parametreRepos;
	@Autowired
	private SolutionRepository solutionRepos;
	
	@Value("${dir.solution}")
	private String solutionDirectory;

	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "redirect:/";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Nos solutions");
			model.addAttribute("active_menu", "solutions");
			
			session.setAttribute("Parametre", parametreRepos.get());
			
			model.addAttribute("Solutions", solutionRepos.findAllItems());
			
			page = "solutions/index";
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return page;
	}
	
	/**
	 * Cette fonction permet de recuperer la photo
	 */
	@GetMapping(value = "/getphoto", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhoto(String filename) throws FileNotFoundException, IOException {
		File file = new File(solutionDirectory + filename); // Je recupère l'image
		if (!file.exists()) {
			// photo par defaut
			String photo_defaut = parametreRepos.findPhotoDefaut();
			file = new File(solutionDirectory + photo_defaut);
		}
		return IOUtils.toByteArray(new FileInputStream(file));
	}



}
