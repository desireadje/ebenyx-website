package com.ebenyx.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpSession;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ebenyx.entities.Parametre;
import com.ebenyx.repositories.ClientEbenyxRepository;
import com.ebenyx.repositories.ParametreRepository;
import com.ebenyx.repositories.PartenaireRepository;

@Controller
@RequestMapping(value = "/about")
public class AboutController {
	
	// Logger
	private static final Logger log = LoggerFactory.getLogger(AboutController.class);
	
	@Autowired
	private ParametreRepository parametreRepos;
	@Autowired
	private ClientEbenyxRepository clientEbenyxRepos;
	@Autowired
	private PartenaireRepository partenaireRepos;
	
	@Value("${dir.brochure}")
	private String brochureDirectory;
	
	@Value("${dir.partenaire}")
	private String partenaireDirectory;

	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "redirect:/";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "A Propos");
			model.addAttribute("active_menu", "about");
			
			session.setAttribute("Parametre", parametreRepos.get());
			
			model.addAttribute("ClientEbenyxs", clientEbenyxRepos.findAllItems());
			
			model.addAttribute("Partenaires", partenaireRepos.findAllItems());
			
			page = "about/index";
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return page;
	}
	
	@GetMapping(value = "/brochure")
	public void downloadFile(HttpSession session,
			HttpServletResponse servletResponse) {
		String filename = null;
		String filepath = null;
		try {
			// Je recupère le nom du fichier
			Parametre p = (Parametre) session.getAttribute("Parametre");
			filename = p.getBrochure();
			// Je format le path du fichier
			filepath = brochureDirectory + filename;

			// J'indique au navigateur de retourner un fichier d'application au lieu d'une page html
			servletResponse.setContentType("application/octet-stream");
			servletResponse.setHeader("Content-Disposition", "attachment;filename=" + filename);

			File file = new File(filepath);
			FileInputStream stream = new FileInputStream(file);
			ServletOutputStream output = servletResponse.getOutputStream();

			byte[] outputByte = new byte[4096];
			// Je copie le contenu binaire dans le flux de sortie
			while (stream.read(outputByte, 0, 4096) != -1) {
				output.write(outputByte, 0, 4096);
			}
			stream.close();
			output.flush();
			output.close();

			log.info("Brochure EBT en cours de téléchargement...");
		} catch (Exception e) {
			log.error("Une erreur s'est produite lors du téléchargement, Veuillez reéssayer plus tard.");
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}
	
	/**
	 * Cette fonction permet de recuperer la photo
	 */
	@GetMapping(value = "/getlogopartenaire", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhoto(String filename) throws FileNotFoundException, IOException {
		File file = new File(partenaireDirectory + filename); // Je recupère l'image
		if (!file.exists()) {
			// photo par defaut
			String photo_defaut = parametreRepos.findPartenaireDefaut();
			file = new File(partenaireDirectory + photo_defaut);
		}
		return IOUtils.toByteArray(new FileInputStream(file));
	}
	
	



}
